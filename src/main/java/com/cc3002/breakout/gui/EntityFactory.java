package com.cc3002.breakout.gui;

import com.almasb.ents.Entity;
import com.almasb.fxgl.entity.EntityView;
import com.almasb.fxgl.entity.GameEntity;
import com.almasb.fxgl.entity.component.CollidableComponent;
import com.almasb.fxgl.physics.BoundingShape;
import com.almasb.fxgl.physics.HitBox;
import com.almasb.fxgl.physics.PhysicsComponent;
import com.cc3002.breakout.gui.controller.BallController;
import com.cc3002.breakout.gui.controller.BatController;
import com.cc3002.breakout.gui.controller.BrickController;
import com.cc3002.breakout.logic.brick.IBrick;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

@SuppressWarnings("restriction")
public class EntityFactory {

  /**
   * Metodo que crea el bat.
   * @param posX posicion de creacion.
   * @param posY posicion de creacon.
   * @return retorna el la entidad bat.
   */
  public static Entity newBat(double posX, double posY) {
    GameEntity bat = new GameEntity();
    bat.getTypeComponent().setValue(EntityType.BAT);
    bat.getPositionComponent().setValue(posX, posY);
    bat.getMainViewComponent().setView(new EntityView(new Rectangle(150,
            25, Color.DARKGREY)), true);
    
    PhysicsComponent batPhysics = new PhysicsComponent();
    batPhysics.setBodyType(BodyType.KINEMATIC);
    
    FixtureDef def = new FixtureDef();
    def.setFriction(1.0f);
    batPhysics.setFixtureDef(def);
    
    bat.addComponent(batPhysics);
    bat.addComponent(new CollidableComponent(true));
    bat.addControl(new BatController());
    
    return bat;
  }

  /**
   * Metodo que crea la pelota del juego.
   * @param posX posicion donde se crea la pelota.
   * @param posY posicion donde se crea la pelota.
   * @return retorna la entidad pelota.
   */
  public static Entity newBall(double posX, double posY) {
    GameEntity ball = new GameEntity();
    ball.getTypeComponent().setValue(EntityType.BALL);
    ball.getPositionComponent().setValue(posX, posY);
    ball.getBoundingBoxComponent().addHitBox(new HitBox("BODY", BoundingShape.circle(10)));
    ball.getMainViewComponent().setView(new Circle(10, Color.WHITE));
    
    final PhysicsComponent ballPhysics = new PhysicsComponent();
    ballPhysics.setBodyType(BodyType.DYNAMIC);
    
    FixtureDef def = new FixtureDef();
    def.setDensity(0.3f);
    def.setRestitution(1.0f);
    
    ballPhysics.setFixtureDef(def);
    ballPhysics.setOnPhysicsInitialized(new Runnable() {
        public void run() {
            ballPhysics.setLinearVelocity(5 * 60,  -5 * 60);
        }
    });
    
    ball.addComponent(ballPhysics);
    ball.addComponent(new CollidableComponent(true));
    ball.addControl(new BallController());
    
    return ball;
  }

  /**
   * Metodo que crea los bricks del juego.
   * @param brick Brick a imprimir.
   * @param contadorX La posicion del brick.
   * @return Retorna un brick del juego
   */
  public static Entity newBricks(IBrick brick, int contadorX) {
    GameEntity bricks = new GameEntity();
    bricks.getTypeComponent().setValue(EntityType.BRICK);
    bricks.getPositionComponent().setValue((contadorX % 16) * 80, (contadorX / 16) * 25);
    bricks.getMainViewComponent().setView(new EntityView(new Rectangle(80, 20)), true);
    if (brick.isIndesctructibleBrick()) {
      bricks.getMainViewComponent().setTexture("ladrillo.jpg");
    } else if (brick.isSoftBrick()) {
      bricks.getMainViewComponent().setTexture("brickVerde.png");
    } else {
      bricks.getMainViewComponent().setTexture("brickNaranjo.png");
    }
    
    PhysicsComponent physics = new PhysicsComponent();
    physics.setBodyType(BodyType.STATIC);
    bricks.addComponent(physics);
    bricks.addComponent(new CollidableComponent(true));
    bricks.addControl(new BrickController(brick));

    return bricks;
  }

}
