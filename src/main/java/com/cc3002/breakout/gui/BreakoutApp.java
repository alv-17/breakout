package com.cc3002.breakout.gui;

import com.almasb.ents.Entity;
import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.entity.Entities;
import com.almasb.fxgl.entity.GameEntity;
import com.almasb.fxgl.entity.component.CollidableComponent;
import com.almasb.fxgl.entity.component.TypeComponent;
import com.almasb.fxgl.input.ActionType;
import com.almasb.fxgl.input.Input;
import com.almasb.fxgl.input.InputMapping;
import com.almasb.fxgl.input.OnUserAction;
import com.almasb.fxgl.physics.CollisionHandler;
import com.almasb.fxgl.physics.HitBox;
import com.almasb.fxgl.settings.GameSettings;
import com.almasb.fxgl.ui.UI;
import com.cc3002.breakout.facade.HomeworkTwoFacade;
import com.cc3002.breakout.gui.controller.BatController;
import com.cc3002.breakout.gui.controller.BrickController;
import com.cc3002.breakout.logic.brick.IBrick;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

@SuppressWarnings("restriction")
public class BreakoutApp extends GameApplication {
  private BatController playerBat;
  private IntegerProperty playerScore;
  private IntegerProperty lives;
  private HomeworkTwoFacade game;

  @Override
  protected void initAssets() {
  //No hace nada.
  }

  @Override
  protected void initGame() {
    playerScore = new SimpleIntegerProperty(0);
    lives = new SimpleIntegerProperty(3);
    game = new HomeworkTwoFacade();

    initParty();
    initBackground();
    initBricks();
    initBat();
    initBall();
    initScreenBounds();
  }
  
  private void initBricks() {
    int contadorX = 0;
    for (IBrick brick: game.getBricks()) {
      Entity bricks = EntityFactory.newBricks(brick, contadorX);
      getGameWorld().addEntity(bricks);
      contadorX++;
    }
  }

  private void initParty() {
    game.newLevelWithSoftAndStoneBricks("Nivel 1", 48, 0.6);
  }

  /**
   * Metodo que inicializa el fondo.
   */
  public void initBackground() {
    GameEntity bg = new GameEntity();
    bg.getMainViewComponent().setView(new Rectangle(getWidth(), getHeight(), Color.rgb(0, 0, 5)));
    getGameWorld().addEntity(bg);
  }
  
  /**
   * Metodo que incializa las murallas.
   */
  public void initScreenBounds() {
    Entity walls = Entities.makeScreenBounds(150);
    walls.addComponent(new TypeComponent(EntityType.WALL));
    walls.addComponent(new CollidableComponent(true));
    
    getGameWorld().addEntity(walls);
  }
  
  public void initBall() {
    getGameWorld().addEntity(EntityFactory.newBall(getWidth() / 2 - 5, getHeight() / 2 - 5));
  }
  
  /**
   * Metodo que inicializa el bat.
   */
  public void initBat() {
    Entity bat = EntityFactory.newBat(550, 600);
    getGameWorld().addEntity(bat);
    playerBat = bat.getControlUnsafe(BatController.class);
  }

  @Override
  protected void initInput() {
    Input input = getInput();
    input.addInputMapping(new InputMapping("Right",KeyCode.RIGHT));
    input.addInputMapping(new InputMapping("Left",KeyCode.LEFT));
  }

  @Override
  protected void initPhysics() {
    getPhysicsWorld().setGravity(0, 0);

    getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.BALL, EntityType.WALL) {
        @Override
        protected void onHitBoxTrigger(Entity entityA, Entity entityB, HitBox boxA, HitBox boxB) {
          if (boxB.getName().equals("TOP")) {
            game.lossOfHeart();
            lives.set(lives.get() - 1);
          }
        }
    });
    
    getPhysicsWorld().addCollisionHandler(new CollisionHandler(EntityType.BALL, EntityType.BRICK) {
        @Override
        protected void onHitBoxTrigger(Entity entityA, Entity entityB, HitBox boxA, HitBox boxB) {
          BrickController brick = entityB.getControlUnsafe(BrickController.class);
          brick.hit();
          if (brick.isDestroyed()) {
            entityB.removeFromWorld();
          }
        }
    });
  }

  @Override
  protected void initUI() {
    AppController controller = new AppController();
    UI ui = getAssetLoader().loadUI("main.fxml", controller);
    
    controller.getLabelScore().textProperty().bind(playerScore.asString("Score: %d"));
    controller.getLabelLife().textProperty().bind(lives.asString("Lives: %d"));

    getGameScene().addUI(ui);
  }

  @Override
  protected void onUpdate(double arg0) {
    //No hace nada.
  }

  @OnUserAction(name = "Right", type = ActionType.ON_ACTION)
  public void right() {
    playerBat.right();
  }

  @OnUserAction(name = "Left", type = ActionType.ON_ACTION)
  public void left() {
    playerBat.left();
  }

  @OnUserAction(name = "Right", type = ActionType.ON_ACTION_END)
  public void stopBat() {
    playerBat.stop();
  }

  @OnUserAction(name = "Left", type = ActionType.ON_ACTION_END)
  public void stopBat2() {
    playerBat.stop();
  }

  @Override
  protected void initSettings(GameSettings settings) {
    settings.setWidth(1280);
    settings.setHeight(720);
    settings.setTitle("Breakout");
    settings.setVersion("0.1");
    settings.setIntroEnabled(false);
    settings.setMenuEnabled(false);
    settings.setFullScreen(false);
    settings.setProfilingEnabled(false);
  }

  public static void main(String[] args) {
    launch(args);
  }

}
