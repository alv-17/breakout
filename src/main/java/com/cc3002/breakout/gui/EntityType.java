package com.cc3002.breakout.gui;

public enum EntityType {
  BAT, BALL, WALL, DOWN_WALL,
  BRICK, SOFT_BRICK, STONE_BRICK, INDESTRUCTIBLE_BRICK
}
