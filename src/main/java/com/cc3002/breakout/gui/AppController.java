package com.cc3002.breakout.gui;

import com.almasb.fxgl.app.FXGL;
import com.almasb.fxgl.ui.UIController;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.util.Duration;

@SuppressWarnings("restriction")
public class AppController implements UIController {
  @FXML
  private Label labelScore;
  
  @FXML
  private Label labelLife;

  public Label getLabelScore() {
    return labelScore;
  }
  
  public Label getLabelLife() {
    return labelLife;
  }
  
  /**
   * Metodo que carga el texto a mostrar.
   */
  public void init() {
    labelScore.setFont(FXGL.getUIFactory().newFont(40));
    labelLife.setFont(FXGL.getUIFactory().newFont(40));
    
    labelLife.layoutBoundsProperty().addListener((observalbe, oldValue, newBounds) -> {
      labelLife.setTranslateX(1000);
      labelLife.setTranslateY(600);
    });
    labelScore.layoutBoundsProperty().addListener((observalbe, oldValue, newBounds) -> {
      labelScore.setTranslateY(600);
    });
    labelScore.textProperty().addListener(new ChangeListener<String>() {
      public void changed(ObservableValue<? extends String> observable,
            String oldValue, String newValue) {
        animatedLabel(labelScore);
      }
    });
    labelLife.textProperty().addListener((observable, oldValue, newValue) -> {
      animatedLabel(labelLife);
    });
    
    
  }
  
  private void animatedLabel(Label label) {
    FadeTransition ft = new FadeTransition(Duration.seconds(0.33), label);
    ft.setFromValue(0);
    ft.setToValue(1);
    ft.play();
  }

}
