package com.cc3002.breakout.gui.controller;

import com.almasb.ents.AbstractControl;
import com.almasb.ents.Entity;
import com.almasb.fxgl.physics.PhysicsComponent;

public class BallController extends AbstractControl {
  private PhysicsComponent ball;

  @Override
  public void onAdded(Entity entity) {
    ball = entity.getComponentUnsafe(PhysicsComponent.class);
  }

  public void onUpdate(Entity arg0, double arg1) {
    limitVelocity();
  }

  /**
   * Metodo que limita la velocidad de la pelota.
   */
  @SuppressWarnings("restriction")
  public void limitVelocity() {
    if (Math.abs(ball.getLinearVelocity().getX()) > 5 * 60) {
      ball.setLinearVelocity(Math.signum(ball.getLinearVelocity().getX()) * 5 * 60, 
          ball.getLinearVelocity().getY());
    }
    
    if (Math.abs(ball.getLinearVelocity().getY()) < 5 * 60 * 2) {
      ball.setLinearVelocity(ball.getLinearVelocity().getX(), 
          Math.signum(ball.getLinearVelocity().getY()) * 5 * 60);
    }
  }
  
}
