package com.cc3002.breakout.gui.controller;

import com.almasb.ents.AbstractControl;
import com.almasb.ents.Entity;
import com.cc3002.breakout.logic.brick.IBrick;

public class BrickController extends AbstractControl {
  private IBrick brick;
  
  public BrickController(IBrick brick) {
    this.brick = brick;
  }

  @Override
  public void onUpdate(Entity arg0, double arg1) {
  }

  public void hit() {
    brick.hit();
  }
  
  public boolean isDestroyed() {
    return brick.isDestroyed();
  }
  
}
