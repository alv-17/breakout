package com.cc3002.breakout.gui.controller;

import com.almasb.ents.AbstractControl;
import com.almasb.ents.Entity;
import com.almasb.fxgl.entity.component.BoundingBoxComponent;
import com.almasb.fxgl.entity.component.PositionComponent;
import com.almasb.fxgl.physics.PhysicsComponent;

public class BatController extends AbstractControl {
  protected PositionComponent position;
  protected PhysicsComponent bat;
  protected BoundingBoxComponent bbox;

  public void onUpdate(Entity arg0, double arg1) { 
  }
  
  @Override
  public void onAdded(Entity entity) {
    bat = entity.getComponentUnsafe(PhysicsComponent.class);
    position = entity.getComponentUnsafe(PositionComponent.class);
    bbox = entity.getComponentUnsafe(BoundingBoxComponent.class);
  }
  
  /**
   * Metodo que mueve el bat a la derecha.
   */
  public void right() {
    if (position.getX() <= 1125) {
      bat.setLinearVelocity(5 * 60, 0);
    } else {
      stop();
    }
  }
  
  /**
   * Metodo que mueve el bat a la izquierda.
   */
  public void left() {
    if (position.getX() <= 5) {
      stop();
    } else {
      bat.setLinearVelocity(-5 * 60, 0);
    }
  }
  
  public void stop() {
    bat.setLinearVelocity(0, 0);
  }

}
