package com.cc3002.breakout.facade;

import com.cc3002.breakout.logic.Game;
import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.level.ILevel;

import java.io.PrintStream;
import java.util.List;

/**
 * Clase que inicializa el breakout.
 * @author alvaro
 *
 */
public class HomeworkTwoFacade {

  private transient Game game;

  /**
   * Metodo que crea un objeto de la clase Game para controlar el juego.
   * @param levelName Nombre del nivel.
   * @param number Numero de ladrillos del nivel.
   * @param probability Probabilidad de ocurrencia de los soft bricks.
   * @return Retorna el Level donde se comenzara el juego.
   */
  public ILevel newLevelWithSoftAndStoneBricks(final String levelName, final int number,
      final double probability) {
    this.game = new Game(number, probability, levelName);
    return game.getLevel();
  }

  public long numberOfBricks() {
    return game.getNumberBricks();
  }

  public List<IBrick> getBricks() {
    return game.getBricks();
  }

  public boolean hasNextLevel() {
    return game.hasNextLevel();
  }

  public String getLevelName() {
    return game.getName();
  }

  public int getRequiredPoints() {
    return game.getNeededPoints();
  }

  public int getNumberOfHearts() {
    return this.game.getLife();
  }

  public int lossOfHeart() {
    this.game.lossLife();
    return 1;
  }

  public long earnedScore() {
    return game.score();
  }

  public ILevel getCurrentLevel() {
    return game.getLevel();
  }

  public void setCurrentLevel(final ILevel newLevel) {
    game.newLevel(newLevel);
  }

  public String spawnBricks(final ILevel level) {
    return game.printBricks(level);
  }
  
  public void setNextLevel() {
    game.nextLevel();
  }
  
  public List<IBonus> newBonuses(final int number, final double probability) {
    return game.setLevelBonuses(number,probability);
    
  }
  
  public void registerBonuses(final List<IBonus> bonuses) {
    game.newBonuses(bonuses);
  }
  
  public void setGameConsoleOutput(final PrintStream printStream) {
    game.setConsole(printStream);
  }
  
  public void autoSwitchToNextLevel() {
    game.autoSwitchToNextLevel();
  }
  
  public boolean hasCurrentLevel() {
    return game.hasLevel();
  }
  
  public IBonus newExtraScore() {
    return game.newExtraScore();
  }
  
  public IBonus newExtraHeart() {
    return game.newExtraHeart();
  }
  
  public IBonus newScoreDiscount() {
    return game.newScoreDiscount();
  }
  
  public IBonus newHeartDiscount() {
    return game.newHeartDiscount();
  }
}
