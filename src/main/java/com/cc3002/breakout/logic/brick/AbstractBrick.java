package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.update.BrickUpdate;

import java.util.Observable;


/**
 * Clase Abstracta que crea bricks.
 * @author alvaro
 *
 */
public abstract class AbstractBrick extends Observable implements IBrick {

  protected transient int life;

  /**
   * Constructor del brick que le asigna una cantidad de vidas.
   * @param life cantidad de vidas del brick.
   */
  public AbstractBrick(final int life) {
    this.life = life;
  }
  
  /**
   * Quita vida y notifica.
   */
  public void hit() {
    this.life--;
    setChanged();
    notifyObservers(new BrickUpdate(this));
  }

  public int remainingHits() {
    return this.life;
  }
  
  public boolean isDestroyed() {
    return this.life < 1;
  }

}
  
