package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;

public class IndestructibleBrick extends AbstractBrick {

  private int points;

  public IndestructibleBrick(int life) {
    super(life);
    this.points = 0;
  }

  @Override
  public boolean isStoneBrick() {
    return false;
  }

  @Override
  public boolean isSoftBrick() {
    return false;
  }

  @Override
  public int getPoints() {
    return this.points;
  }

  @Override
  public void printBrick(Printer printer, StringBuffer stream) {
    printer.printIndestructibleBrick(stream);
  }

  @Override
  public void printBrickText(GameConsole console) {
    //No hace nada.
  }

  @Override
  public boolean isIndesctructibleBrick() {
    return true;
  }

}
