package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;

/**
 * Crea un objeto Soft Brick para el juego breakout.
 * 
 * @author alvaro
 *
 */
public class SoftBrick extends AbstractBrick implements IBrick {

  private final transient int points;

  /**
   * Constructor de la clase SoftBrick que llama al constructor super.
   * 
   * @param life vida con la que empieza el brick.
   */
  public SoftBrick(final int life) {
    super(life);
    this.points = 10;
  }

  public boolean isStoneBrick() {
    return false;
  }

  public boolean isSoftBrick() {
    return true;
  }

  public int getPoints() {
    return this.points;
  }

  public void printBrick(final Printer printer, final StringBuffer stream) {
    printer.printSoftBrick(stream);
  }

  public void printBrickText(GameConsole printer) {
    printer.printSoftBrickText();
  }

  @Override
  public boolean isIndesctructibleBrick() {
    return false;
  }
  

}
