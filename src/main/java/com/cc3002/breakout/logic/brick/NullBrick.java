package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;

/**
 * Clase que representa un no brick.
 * @author alvaro.
 *
 */
public class NullBrick extends AbstractBrick implements IBrick {

  public NullBrick() {
    super(0);
  }

  public boolean isStoneBrick() {
    return false;
  }

  public boolean isSoftBrick() {
    return false;
  }

  @Override
  public void hit() {
  //No hace nada
  }

  @Override
  public int remainingHits() {
    return 0;
  }

  public int getPoints() {
    return 0;
  }

  public void printBrick(Printer printer, StringBuffer stream) {
    printer.printNullBrick(stream);
  }

  public boolean isDestroyed() {
    return true;
  }

  public void printBrickText(GameConsole printer) {
  //No printea nada
  }

  @Override
  public boolean isIndesctructibleBrick() {
    return false;
  }

}
