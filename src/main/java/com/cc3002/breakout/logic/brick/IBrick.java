package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;

/**
 * Interfaz que define metodos para los bricks.
 * @author alvaro
 *
 */
public interface IBrick {

  boolean isStoneBrick();

  boolean isSoftBrick();

  boolean isIndesctructibleBrick();
  
  void hit();

  int remainingHits();

  int getPoints();

  void printBrick(Printer printer, StringBuffer stream);
  
  boolean isDestroyed();

  void printBrickText(GameConsole console);
}
