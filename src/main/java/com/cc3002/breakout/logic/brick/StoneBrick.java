package com.cc3002.breakout.logic.brick;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;

/**
 * Clase Brick que nos permite crear Stone Bricks para el juego breakout.
 * 
 * @author Alvaro.
 */
public class StoneBrick extends AbstractBrick implements IBrick {
  
  private final transient int points;

  /**
   * Constructor de la clase StoneBrick que llama al constructor super.
   * 
   * @param life vida con la que inicia el brick
   */
  public StoneBrick(final int life) {
    super(life);
    this.points = 50;
  }

  public boolean isStoneBrick() {
    return true;
  }

  public boolean isSoftBrick() {
    return false;
  }

  public int getPoints() {
    return this.points;
  }

  public void printBrick(final Printer printer, final StringBuffer stream) {
    printer.printStoneBrick(stream);
    
  }

  public void printBrickText(GameConsole printer) {
    printer.printStoneBrickText();
  }

  @Override
  public boolean isIndesctructibleBrick() {
    return false;
  }

}
