package com.cc3002.breakout.logic;

import com.cc3002.breakout.logic.brick.IBrick;

import java.util.Iterator;
import java.util.List;


/**
 * Clase que imprime lista de bircks.
 * @author alvaro
 *
 */
public class Printer {
  
  private static final String SOFT = "*";
  private static final String STONE = "#";
  private static final String NULL = " ";
  private static final String INDESTRUCTIBLE = "+";
  
  /**
   * Metodo que imprime los bricks de una lista.
   * @param brickList Lista de bricks a imprimir.
   * @return retorna la lista de bricks como String.
   */
  public String print(final List<IBrick> brickList) {
    final StringBuffer stream = new StringBuffer();
    final Iterator<IBrick> iterator = brickList.iterator();
    int contador = 16;
    while (iterator.hasNext()) {
      iterator.next().printBrick(this, stream);
      if (contador - 1 == 0) {
        stream.append(System.lineSeparator());
        contador = 17;
      }
      contador--;
    }
    return stream.toString();
  }
  
  public void printStoneBrick(final StringBuffer stream) {
    stream.append(STONE);
  }
  
  public void printSoftBrick(final StringBuffer stream) {
    stream.append(SOFT);
  }

  public void printNullBrick(final StringBuffer stream2) {
    stream2.append(NULL);
  }

  public void printIndestructibleBrick(StringBuffer stream) {
    stream.append(INDESTRUCTIBLE);
  }

}
