package com.cc3002.breakout.logic.update;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.bonus.IBonus;

/**
 * Clase que dice que el mensaje fue de un bono.
 * @author alvaro.
 *
 */
public class BonusUpdate implements IUpdate {

  private final IBonus bonus;

  /**
   * Constructor que guarda el tipo de bnus que lo invoco.
   * @param bonus Que tipo de bono lo invoco.
   */
  public BonusUpdate(IBonus bonus) {
    this.bonus = bonus;
  }

  public void printConsole(GameConsole printer) {
    printer.printBonusUpdate(this.bonus);
  }

  public boolean isBonusUpdate() {
    return true;
  }

  public int getScore() {
    return this.bonus.getScore();
  }

  public boolean isBrickUpdate() {
    return false;
  }
  
  public IBonus getBonus() {
    return this.bonus;
  }

}
