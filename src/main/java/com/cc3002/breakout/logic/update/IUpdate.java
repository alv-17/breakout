package com.cc3002.breakout.logic.update;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Interfaz que controla las actualizaciones.
 * @author alvaro.
 *
 */
public interface IUpdate {

  public void printConsole(GameConsole console);
  
  public boolean isBonusUpdate();
  
  public boolean isBrickUpdate();
  
  public int getScore();

}
