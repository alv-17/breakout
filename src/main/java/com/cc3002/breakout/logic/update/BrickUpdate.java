package com.cc3002.breakout.logic.update;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.brick.IBrick;

/**
 * Clase que dice que el mensaje fue de un brick.
 * @author alvaro.
 *
 */
public class BrickUpdate implements IUpdate {

  private final IBrick brick;

  /**
   * Constructor que guarda el brick que lo invico.
   * @param brick Que tipo de brick lo invoco.
   */
  public BrickUpdate(IBrick brick) {
    this.brick = brick;
  }

  public void printConsole(GameConsole printer) {
    printer.printBrickUpdate(brick);
  }

  public boolean isBonusUpdate() {
    return false;
  }

  public int getScore() {
    return this.brick.getPoints();
  }
  
  public IBrick getBrick() {
    return this.brick;
  }

  public boolean isBrickUpdate() {
    return true;
  }

}
