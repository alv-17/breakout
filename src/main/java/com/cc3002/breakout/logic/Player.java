package com.cc3002.breakout.logic;

/**
 * Clase Player que jugara el juego breakout.
 * 
 * @author alvaro.
 *
 */
public class Player {
  private transient int life;
  private transient long score;

  /**
   * Constructor que crea un jugador con sus respectivas vidas y puntaje.
   */
  public Player() {
    this.life = 3;
    this.score = 0;
  }

  public void lossLife() {
    this.life--;
  }

  public void earnScore(final int moreScore) {
    this.score += moreScore;
  }

  public int getLife() {
    return this.life;
  }

  public long getScore() {
    return this.score;
  }

  public void gainedLife() {
    this.life++;
  }

}
