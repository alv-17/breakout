package com.cc3002.breakout.logic.level;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.brick.IndestructibleBrick;
import com.cc3002.breakout.logic.brick.NullBrick;
import com.cc3002.breakout.logic.brick.SoftBrick;
import com.cc3002.breakout.logic.brick.StoneBrick;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

/**
 * Clase que contendra los ladrillos del juego y su distribucion.
 * 
 * @author alvaro.
 *
 */
public class Level extends Observable implements ILevel, Observer {

  private ILevel nextLevel = new NullLevel();
  private final String name;
  private transient List<IBrick> listBricks;
  private transient int points;
  private List<IBonus> bonuses;
  private int requiredPoints;
  private int numberBricks;
  
  /**
   * Constructor de la clase Level, que genera un nivel asignandole sus respectivos parametros.
   * 
   * @param numBricks Numero de ladrillos del nivel.
   * @param probability Probabilidad de ocurrencia de bricks tipo soft.
   * @param name2 Nombre del nivel.
   */
  public Level(final int numBricks, final double probability, final String name2) {
    this.name = name2;
    insertBricks(probability, numBricks);
  }


  /**
   * Inserta los bricks requeridos en la lista.
   * 
   * @param probability probabilidad de ocurrencia de ladrillos Soft.
   * @param numBricks numero de bricks del nivel
   */

  public void insertBricks(final double probability, final int numBricks) {
    this.numberBricks = numBricks;
    int auxiliar = numBricks;
    final Random rand = new Random();
    listBricks = new ArrayList<IBrick>(numBricks);
    this.points = 0;
    while (auxiliar != 0) {
      if (rand.nextDouble() <= probability) {
        SoftBrick soft = new SoftBrick(1);
        soft.addObserver(this);
        listBricks.add(soft);
        this.points += 10;
      } else if (rand.nextDouble() <= 0.8) {
        StoneBrick stone = new StoneBrick(3);
        stone.addObserver(this);
        listBricks.add(stone);
        this.points += 50;
      } else {
        IndestructibleBrick brick = new IndestructibleBrick(1000);
        brick.addObserver(this);
        listBricks.add(brick);
      }
      auxiliar--;
    }
    this.requiredPoints = (int) (this.points * 0.7);
  }

  public String getName() {
    return this.name;
  }

  public List<IBrick> getBricks() {
    return this.listBricks;
  }


  public int getPoints() {
    return this.points;
  }
  
  public ILevel getNextLevel() {
    return this.nextLevel;
  }


  public void setNextLevel(ILevel level) {
    this.nextLevel = level;
  }


  public void setRequiredPoints(int points) {
    this.requiredPoints = points;
  }

  public void update(Observable observed, Object arg) {
    setChanged();
    notifyObservers(arg);
  }


  public boolean isNullLevel() {
    return false;
  }


  public void newBonuses(List<IBonus> bonuses2) {
    this.bonuses = bonuses2;
  }


  public List<IBonus> getBonuses() {
    return this.bonuses;
  }
  
  public int getRequiredPoints() {
    return this.requiredPoints;  
  }


  public int getNumberOfBricks() {
    return this.numberBricks;
  }


  public boolean isLevel() {
    return true;
  }


  /**
   * Metodo que cuenta cuantos bricks contretos quedan.
   */
  public long countBricks() {
    int contador = 0;
    for (IBrick brick: this.listBricks) {
      if (brick.isDestroyed()) {
        brick = new NullBrick();
      } else {
        contador ++;
      }
    }
    return contador;
  }

}
