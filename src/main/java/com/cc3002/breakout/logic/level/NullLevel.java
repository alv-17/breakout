package com.cc3002.breakout.logic.level;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;

import java.util.List;

/**
 * Clase que representa un no level.
 * @author alvaro.
 *
 */
public class NullLevel implements ILevel {

  public String getName() {
    return null;
  }

  public void insertBricks(double probability, int numBricks) {
  //Metodo Vacio
  }

  public List<IBrick> getBricks() {
    return null;
  }

  public int getPoints() {
    return 0;
  }

  public void setNextLevel(ILevel level) {
  //Metodo Vacio
  }

  public void setRequiredPoints(int points) {
  //Metodo Vacio
  }

  public boolean isNullLevel() {
    return true;
  }

  public ILevel getNextLevel() {
    return null;
  }

  public void newBonuses(List<IBonus> bonuses2) {
    //No hace nada
  }

  public List<IBonus> getBonuses() {
    return null;
  }

  public int getNumberOfBricks() {
    return 0;
  }

  public boolean isLevel() {
    return false;
  }

  public long countBricks() {
    return 0;
  }

}
