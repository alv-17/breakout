package com.cc3002.breakout.logic.level;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;

import java.util.List;

/**
 * Interfaz que define metodos para Level.
 * @author alvaro
 *
 */
public interface ILevel {

  String getName();
  
  void insertBricks(double probability, int numBricks);
  
  List<IBrick> getBricks();

  int getPoints();
  
  void setNextLevel(ILevel level);
  
  void setRequiredPoints(int points);

  boolean isNullLevel();

  ILevel getNextLevel();

  void newBonuses(List<IBonus> bonuses2);

  List<IBonus> getBonuses();

  int getNumberOfBricks();
  
  boolean isLevel();

  long countBricks();

}
