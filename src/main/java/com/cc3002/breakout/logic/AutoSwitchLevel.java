package com.cc3002.breakout.logic;

import com.cc3002.breakout.logic.update.BonusUpdate;
import com.cc3002.breakout.logic.update.BrickUpdate;
import com.cc3002.breakout.logic.update.IUpdate;

import java.util.Observable;
import java.util.Observer;

/**
 * Clase que observa el juego y cambia automaticamente el nivel.
 * @author alvaro.
 *
 */
public class AutoSwitchLevel implements Observer {

  private GameConsole console;
  private int levelScore = 0;

  /**
   * Funcion que suma puntos dependiendo del tipo de actualizacion.
   * @param obs Objeto que manda el mensaje.
   * @param arg Parametro del mensaje.
   */
  public void update(Observable obs, Object arg) {
    if (arg == null) {
      autoSwitchToNextLevel(obs);
    } else {
      getScore(arg,obs);
      autoSwitchToNextLevel(obs);
    }
  }

  public boolean check(Observable obs) {
    Game game = (Game) obs;
    return this.levelScore > game.getNeededPoints();
  }

  /**
   * Metodo que cambia el nivel de cumplirse la condicion.
   * @param obs Objeto observado.
   */
  public void autoSwitchToNextLevel(Observable obs) {
    if (check(obs)) {
      Game game = (Game) obs;
      this.console.printNewLevel(game.getLevel());
      game.nextLevel();
    }
  }

  /**
   * Funcion que incrementa les punts del nivel y el juego.
   * @param arg Objeto del cual se extraera los puntos a sumar.
   */
  public void getScore(Object arg, Observable obs) {
    Game game = (Game) obs;
    IUpdate update = (IUpdate) arg;
    if (update.isBonusUpdate()) {
      BonusUpdate bonus = (BonusUpdate) update;
      this.levelScore += bonus.getScore();
      game.earnedScore(bonus.getScore());
    } else {
      BrickUpdate brick = (BrickUpdate) update;
      this.levelScore += brick.getScore();
      game.earnedScore(brick.getScore());
    }
  }

  public void setConsole(GameConsole console) {
    this.console = console;
  }

}
