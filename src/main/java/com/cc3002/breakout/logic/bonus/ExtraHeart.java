package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Clase que agrega extra vidas.
 * @author alvaro.
 *
 */
public class ExtraHeart extends Bonus {

  /**
   * Constructor que llama a super.
   */
  public ExtraHeart() {
    super(1);
  }

  public boolean isExtraBonus() {
    return true;
  }

  public boolean isDiscount() {
    return false;
  }

  public void printBonus(GameConsole printer) {
    printer.printExtraHeart();
  }

  public boolean isHeart() {
    return true;
  }

  public boolean isScore() {
    return false;
  }

  public int getScore() {
    return 0;
  }

}
