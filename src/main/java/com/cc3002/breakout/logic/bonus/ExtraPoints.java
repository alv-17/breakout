package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Clase que representa aumento de puntaje.
 * @author alvaro.
 *
 */
public class ExtraPoints extends Bonus {

  /**
   * Constructor que llama a super.
   */
  public ExtraPoints() {
    super(5);
  }

  public boolean isExtraBonus() {
    return true;
  }

  public boolean isDiscount() {
    return false;
  }

  public void printBonus(GameConsole printer) {
    printer.printExtraPoints();
  }

  public boolean isHeart() {
    return false;
  }

  public boolean isScore() {
    return true;
  }

  public int getScore() {
    return 5;
  }

}
