package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Clase que representa un descuento de puntaje.
 * @author alvaro.
 *
 */
public class DiscountPoints extends Bonus {

  /**
   * Constructos que llama a super.
   */
  public DiscountPoints() {
    super(3);
  }

  public boolean isExtraBonus() {
    return false;
  }

  public boolean isDiscount() {
    return true;
  }

  public void printBonus(GameConsole printer) {
    printer.printDiscountPoints();
  }

  public boolean isHeart() {
    return false;
  }

  public boolean isScore() {
    return true;
  }

  public int getScore() {
    return -3;
  }

}
