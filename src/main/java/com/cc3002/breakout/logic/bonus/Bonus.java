package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.update.BonusUpdate;

import java.util.Observable;

/**
 * Clase abstracta bonus, puede ser bonus extra o descuento.
 * @author alvaro.
 *
 */
public abstract class Bonus extends Observable implements IBonus {

  private final int bonus;
  
  /**
   * Constructor ue setea la cantidad de aumento o disminucion de caracteristica.
   * @param bonus descuento o aunmento de caracteristica.
   */
  public Bonus(int bonus) {
    this.bonus = bonus;
  }

  public void reached() {
    setChanged();
    notifyObservers(new BonusUpdate(this));
  }
  
  public int getBonus() {
    return this.bonus;
  }

}
