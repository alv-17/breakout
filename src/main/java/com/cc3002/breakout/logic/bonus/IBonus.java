package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Interfaz que regula el comportamiento de los bonus.
 * @author alvaro.
 *
 */
public interface IBonus {

  void reached();
  
  boolean isExtraBonus();
  
  boolean isDiscount();

  void printBonus(GameConsole console);
  
  boolean isHeart();
  
  boolean isScore();

  int getScore();
}
