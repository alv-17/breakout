package com.cc3002.breakout.logic.bonus;

import com.cc3002.breakout.logic.GameConsole;

/**
 * Clase que descuenta corazones.
 * @author alvaro.
 *
 */
public class DiscountHeart extends Bonus {

  /**
   * Lama al constructor super.
   */
  public DiscountHeart() {
    super(1);
  }

  public boolean isExtraBonus() {
    return false;
  }

  public boolean isDiscount() {
    return true;
  }

  public void printBonus(GameConsole printer) {
    printer.printDiscountHeart();
  }

  public boolean isHeart() {
    return true;
  }

  public boolean isScore() {
    return false;
  }

  public int getScore() {
    return 0;
  }

}
