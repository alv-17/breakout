package com.cc3002.breakout.logic;

import com.cc3002.breakout.logic.bonus.DiscountHeart;
import com.cc3002.breakout.logic.bonus.DiscountPoints;
import com.cc3002.breakout.logic.bonus.ExtraHeart;
import com.cc3002.breakout.logic.bonus.ExtraPoints;
import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.level.ILevel;
import com.cc3002.breakout.logic.level.Level;
import com.cc3002.breakout.logic.update.BonusUpdate;
import com.cc3002.breakout.logic.update.BrickUpdate;
import com.cc3002.breakout.logic.update.IUpdate;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;


/**
 * Clase juego que controla el juego.
 * @author alvaro
 *
 */
public class Game extends Observable implements Observer {

  private PrintStream printStream;
  private final transient Player player;
  private transient ILevel currentLevel;
  private final GameConsole console;
  
  /**
   * Constructor de la clase juego que asigna nivel.
   * @param numberOfBricks numero de bricks del nivel actual.
   * @param probability probbilidad de ocurrencia de soft bricks.
   * @param name nombre del nivel actual.
   */
  public Game(final int numberOfBricks,final double probability, final String name) {
    this.currentLevel = new Level(numberOfBricks,probability,name);
    this.player = new Player();
    this.console = new GameConsole();
    AutoSwitchLevel switchLevel = new AutoSwitchLevel();
    switchLevel.setConsole(console);
    this.addObserver(switchLevel);
  }

  public ILevel getLevel() {
    return this.currentLevel;
  }
  
  public void earnedScore(final int points) {
    this.player.earnScore(points);
  }

  public long getNumberBricks() {
    return this.currentLevel.countBricks();
  }

  public List<IBrick> getBricks() {
    return this.currentLevel.getBricks();
  }

  public String getName() {
    return this.currentLevel.getName();
  }

  public int getLife() {
    return this.player.getLife();
  }

  public void lossLife() {
    this.player.lossLife();
  }

  public long score() {
    return this.player.getScore();
  }

  public boolean hasNextLevel() {
    return !this.currentLevel.getNextLevel().isNullLevel();
  }

  public int getNeededPoints() {
    return (int) (this.currentLevel.getPoints() * 0.7 );
  }

  public void newLevel(final ILevel newLevel) {
    this.currentLevel = newLevel;
  }
  
  public void addNextLevel(ILevel level) {
    this.currentLevel.setNextLevel(level);
  }
  
  /**
   * Retorna los bricks como string.
   * @param level el nivel del cual se quieren imprimir los bricks.
   * @return retorna el string con los bricks del nivel.
   */
  public String printBricks(final ILevel level) {
    final Printer printer = new Printer();
    return printer.print(level.getBricks());
  }

  /**
   * Notifica que se rompio un brick o se emitio un bonus.
   * @param observed referencia al bjeto observado.
   * @param arg Objeto que se le pasa como parametro.
   */
  public void update(Observable observed, Object arg) {
    IUpdate update = (IUpdate) arg;
    if (update.isBrickUpdate()) {
      BrickUpdate brick = (BrickUpdate) update;
      if (brick.getBrick().isDestroyed()) {
        console.printConsole(arg);
        bonusChoice();
      }
    } else {
      BonusUpdate bonus = (BonusUpdate) update;
      console.printConsole(arg);
      if (bonus.getBonus().isHeart() && bonus.getBonus().isDiscount()) {
        this.player.lossLife();
      } else {
        this.player.gainedLife();
      }
    }
    setChanged();
    notifyObservers(arg);
  }

  private void bonusChoice() {
    Random rand = new Random();
    if (rand.nextDouble() 
         < this.currentLevel.getBonuses().size() / this.currentLevel.getNumberOfBricks()) {
      this.currentLevel.getBonuses().get(0).reached();
      this.currentLevel.getBonuses().remove(0);
    }
  }

  public boolean hasLevel() {
    return !currentLevel.isNullLevel();
  }

  public void setConsole(PrintStream printStream) {
    this.printStream = printStream;
    this.console.addConsole(printStream);
  }

  /**
   * Genera la lista con bonos.
   * @param number numero de bonus.
   * @param probability probabilidad de extra bonus.
   */
  public List<IBonus> addBonuses(final int number, final double probability) {
    int aux = number;
    final Random rand = new Random();
    List<IBonus> bonuses = new ArrayList<IBonus>();
    while (aux != 0) {
      if (rand.nextDouble() <= probability) {
        if (rand.nextDouble() < 0.7) {
          ExtraPoints exPoints = new ExtraPoints();
          exPoints.addObserver(this);
          bonuses.add(exPoints);
        } else {
          ExtraHeart exHeart = new ExtraHeart();
          exHeart.addObserver(this);
          bonuses.add(exHeart);
        }
      } else {
        if (rand.nextDouble() < 0.7) {
          DiscountPoints disPoints = new DiscountPoints();
          disPoints.addObserver(this);
          bonuses.add(disPoints);
        } else {
          DiscountHeart disHeart = new DiscountHeart();
          disHeart.addObserver(this);
          bonuses.add(disHeart);
        }
      }
      aux--;
    }
    return bonuses;
  }
  
  public List<IBonus> setLevelBonuses(final int number, final double probability) {
    this.currentLevel.newBonuses(addBonuses(number,probability));
    return this.currentLevel.getBonuses();
  }

  public List<IBonus> getBonuses() {
    return this.currentLevel.getBonuses();
  }

  public void newBonuses(List<IBonus> bonuses2) {
    this.currentLevel.newBonuses(bonuses2);
  }

  public IBonus newScoreDiscount() {
    return new DiscountPoints();
  }

  public IBonus newHeartDiscount() {
    return new DiscountHeart();
  }

  public IBonus newExtraHeart() {
    return new ExtraHeart();
  }

  public IBonus newExtraScore() {
    return new ExtraPoints();
  }

  public void nextLevel() {
    this.currentLevel = currentLevel.getNextLevel();
  }

  public void autoSwitchToNextLevel() {
    notifyObservers();
  }
  
  public PrintStream getConsole() {
    return this.printStream;
  }
  
}
