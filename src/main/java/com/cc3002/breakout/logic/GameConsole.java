package com.cc3002.breakout.logic;

import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.level.ILevel;
import com.cc3002.breakout.logic.update.IUpdate;

import java.io.PrintStream;

/**
 * Clase que se encarga de imprimir mensajes en la consola.
 * @author alvaro.
 */
public class GameConsole {
  
  private PrintStream stream;
  
  public void addConsole(PrintStream printer) {
    this.stream = printer;
  }
  
  public void printBonusUpdate(IBonus bonus) {
    bonus.printBonus(this);
  }

  public void printBrickUpdate(IBrick brick) {
    brick.printBrickText(this);
  }

  public void printSoftBrickText() {
    stream.println("Soft brick destroyed and gained 10 points." + System.lineSeparator());
  }

  public void printStoneBrickText() {
    stream.println("Stone brick destroyed and gained 50 points." + System.lineSeparator());
  }

  public void printDiscountHeart() {
    stream.println("Heart discount emitted." + System.lineSeparator());
  }

  public void printDiscountPoints() {
    stream.println("Score discount emitted." + System.lineSeparator());
  }

  public void printExtraHeart() {
    stream.println("Extra heart bonus emitted." + System.lineSeparator());
  }

  public void printExtraPoints() {
    stream.println("Extra score bonus emitted." + System.lineSeparator());
  }
  
  public void printConsole(Object obj) {
    IUpdate update = (IUpdate) obj;
    update.printConsole(this);
  }

  public void printNewLevel(ILevel level) {
    stream.println("Playing level " + level.getName() + "." + System.lineSeparator());
  }
}
