package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.bonus.ExtraPoints;
import com.cc3002.breakout.logic.brick.StoneBrick;
import com.cc3002.breakout.logic.update.BonusUpdate;
import com.cc3002.breakout.logic.update.BrickUpdate;

import org.junit.Before;
import org.junit.Test;

public class UpdateTest {

  private transient BrickUpdate brick;
  private transient BonusUpdate bonus;

  @Before
  public void setUp() {
    brick = new BrickUpdate(new StoneBrick(3));
    bonus = new BonusUpdate(new ExtraPoints());
  }
  
  @Test
  public void testGetBpnus() {
    assertTrue("Bonus de extra bonus", bonus.getBonus().isExtraBonus());
  }
  
  @Test
  public void testGetScore() {
    assertEquals("Me da 50 puntos",50,brick.getScore());
  }

  @Test
  public void testGetScore2() {
    assertEquals("Me da 5 puntos",5,bonus.getScore());
  }
  
  @Test
  public void testIsBonusUpdate() {
    assertFalse("NO es bonus",brick.isBonusUpdate());
  }
  
  @Test
  public void testIsBonusUpdate2() {
    assertTrue("Es bonus",bonus.isBonusUpdate());
  }
  
  @Test
  public void testIsBrickUpdate() {
    assertTrue("Es bonus",brick.isBrickUpdate());
  }
  
  @Test
  public void testIsBrickUpdate2() {
    assertFalse("NO es bonus",bonus.isBrickUpdate());
  }
  
  @Test
  public void testGetBrick() {
    assertTrue("Da un stone brick",brick.getBrick().isStoneBrick());
  }
}
