package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.facade.HomeworkTwoFacade;
import com.cc3002.breakout.logic.Game;
import com.cc3002.breakout.logic.bonus.IBonus;
import com.cc3002.breakout.logic.level.ILevel;
import com.cc3002.breakout.logic.level.Level;

import org.junit.Before;
import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;

public class HomeworkTwoFacadeTest {
  private transient HomeworkTwoFacade theGame;
  private transient ILevel level;
  private transient Game aux;
  private transient PrintStream print;

  /**
   * Inicializa los argumentos.
   */
  @Before
  public void setUp() {
    theGame = new HomeworkTwoFacade();
    level = theGame.newLevelWithSoftAndStoneBricks("Nivel 1", 10, 1);
    aux = new Game(10, 1, "Nivel");
    print = null;
  }

  @Test
  public void testCreation() {
    assertNotNull("Debe ser no nulo el objeto", theGame);
  }

  @Test
  public void testNewLevelWithSoftAndStoneBricks() {
    assertNotNull("El nivel debe ser no nulo", level);

  }

  @Test
  public void testNumberOfBricks() {
    assertEquals("Hay 10 ladrillos en el juego", 10, theGame.numberOfBricks());
  }

  @Test
  public void testGetBricks() {
    assertEquals("Tiene los mismos bricks", level.getBricks(), theGame.getBricks());
  }

  @Test
  public void testGetLevelName() {
    assertEquals("El nivel se llama Nivel 1", "Nivel 1", theGame.getLevelName());
  }

  @Test
  public void testGetRequiredPoints() {
    assertEquals("Los puntos para pasar el nivel son 70", 70, theGame.getRequiredPoints());
  }

  @Test
  public void testGetNumberOfHearts() {
    assertEquals("Tiene 3 vidas", 3, theGame.getNumberOfHearts());
  }

  @Test
  public void testLossOfHeart() {
    assertEquals("Pierde una vida", 1, theGame.lossOfHeart());
  }

  @Test
  public void testLossOfHeart2() {
    theGame.lossOfHeart();
    assertEquals("Quedan 2 vidas", 2, theGame.getNumberOfHearts());
  }

  @Test
  public void testEarnedScore() {
    assertEquals("Tiene 0 puntos", 0, theGame.earnedScore());
  }

  @Test
  public void testGetCurrentLevel() {
    assertEquals("Son el mismo nivel", level, theGame.getCurrentLevel());
  }

  @Test
  public void testGetCurrentLevel2() {
    assertEquals("El nombre del nivel actual es Nivel 1", "Nivel 1",
        theGame.getCurrentLevel().getName());
  }

  @Test
  public void testSetCurrentLevel() {
    theGame.setCurrentLevel(new Level(10, 0, "Nivel de prueba"));
    assertEquals("El nivel ahora es Nivel de prueba", "Nivel de prueba", theGame.getLevelName());
  }

  @Test
  public void testHasNextLevel() {
    assertFalse("No hay mas niveles", theGame.hasNextLevel());
  }

  @Test
  public void testSpawnBriks() {
    assertEquals("Los bricks deben ser **********", "**********", theGame.spawnBricks(level));
  }
  
  @Test
  public void testSetNextLevel() {
    theGame.setNextLevel();
    assertTrue("El siguiente nivel debe ser NullLevel",theGame.getCurrentLevel().isNullLevel());
  }
  
  @Test
  public void testNewBonuses() {
    assertTrue("Debe dar un discount",theGame.newBonuses(10, 0).get(0).isDiscount());
  }
  
  @Test
  public void testNewBonuses2() {
    assertTrue("Debe dar un bonus extra",theGame.newBonuses(10, 1).get(0).isExtraBonus());
  }
  
  @Test
  public void testRegirterBonuses() {
    ArrayList<IBonus> list = new ArrayList<IBonus>();
    aux.newBonuses(list);
    theGame.registerBonuses(list);
    assertEquals("Deben ser las mismas listas",0,aux.getBonuses().size()); 
  }
  
  @Test
  public void testSetGameConsoleOutput() {
    theGame.setGameConsoleOutput(print);
    aux.setConsole(print);
    assertNull("La consola debe ser nula", aux.getConsole());
  }
  
  @Test
  public void testHasCurrentLevel() {
    assertTrue("Debe tener nivel actual",theGame.hasCurrentLevel());
  }
  
  @Test
  public void testNewExtraHeart() {
    assertTrue("Debe dar un bonus de corazon",theGame.newExtraHeart().isHeart());
  }
  
  @Test
  public void testNewExtraScore() {
    assertTrue("Debe dar un bonus de puntos",theGame.newExtraScore().isExtraBonus());
  }
  
  @Test
  public void testNewHeartDiscount() {
    assertNotNull("Debe dar un descuento de vida",theGame.newHeartDiscount().isDiscount());
  }

  @Test
  public void testNewScoreDiscount() {
    assertTrue("Debe dar un descuento de puntos",theGame.newScoreDiscount().isScore());
  }
  
  @Test
  public void testAutoSwitchToNextLevel() {
    theGame.autoSwitchToNextLevel();
    assertFalse("No cambio el nivel",theGame.getCurrentLevel().isNullLevel());
  }
}








