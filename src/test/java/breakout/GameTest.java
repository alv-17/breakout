package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.Game;
import com.cc3002.breakout.logic.level.Level;

import org.junit.Before;
import org.junit.Test;

public class GameTest {
  private transient Game game;

  @Before
  public void setUp() {
    game = new Game(10, 0, "Nivel 1");
  }

  @Test
  public void testCreation() {
    assertNotNull("El objeto es no nulo", game);
  }

  @Test
  public void testGetLevel() {
    assertEquals("El nombre del nivel debe ser Nivel11", "Nivel 1", game.getLevel().getName());
  }

  @Test
  public void testGetLife() {
    assertEquals("Debe tener 3 vidas", 3, game.getLife());
  }

  @Test
  public void testGetName() {
    assertEquals("El nivel debe llamarse Nivel 1", "Nivel 1", game.getName());
  }

  @Test
  public void testPrintBricks() {
    assertEquals("Debe dar ##########", "##########", game.printBricks(game.getLevel()));
  }

  @Test
  public void testGetNumberBricks() {
    assertEquals("Debe haber 10 ladrillos en el nivel", 10, game.getNumberBricks());
  }

  @Test
  public void testScore() {
    assertEquals("Debe tener 0 puntos", 0, game.score());
  }

  @Test
  public void testGetTotalPoints() {
    final int points = (int) (500 * 0.7);
    assertEquals("Debe tener 500 puntos el nivel", points, game.getNeededPoints());
  }

  @Test
  public void testLossLife() {
    game.lossLife();
    assertEquals("Deben quedarle 2 vidas", 2, game.getLife());
  }

  @Test
  public void testGetBricks() {
    assertEquals("Debe haber 10 ladrillos en la lista", 10, game.getBricks().size());
  }

  @Test
  public void testNewLevel() {
    game.newLevel(new Level(10, 0, "Nivel 2"));
    assertEquals("El nuevo nivel debe llamarse Nivel 2", "Nivel 2", game.getName());
  }

  @Test
  public void testHasNextLevel() {
    assertFalse("No hay mas niveles", game.hasNextLevel());
  }
  
  @Test
  public void testAddNextLevel() {
    game.addNextLevel(new Level(0, 0, "B"));
    assertTrue("No es un nivel nulo",game.hasNextLevel());
  }
  
  @Test
  public void testHasLevel() {
    assertTrue("Tiene nivel",game.hasLevel());
  }
  
  @Test
  public void testHasLevel2() {
    game.nextLevel();
    assertFalse("No tiene nivel",game.hasLevel());
  }

}
