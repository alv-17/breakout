package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.Printer;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.brick.SoftBrick;

import org.junit.Before;
import org.junit.Test;


public class SoftBrickTest {
  private transient IBrick soft;
  
  @Before
  public void setUp() {
    soft = new SoftBrick(1);
  }

  @Test
  public void testCreation() {
    assertNotNull("No es nulo", soft);
  }

  @Test
  public void testRemainingHits() {
    assertEquals("Tiene 1 vida", soft.remainingHits(), 1);
  }

  @Test
  public void testIsStoneBrick() {
    assertFalse("False porque es un soft brick", soft.isStoneBrick());
  }

  @Test
  public void testIsSoftBrick() {
    assertTrue("True porque es un soft brick", soft.isSoftBrick());
  }

  @Test
  public void testHit() {
    soft.hit();
    assertEquals("Le quedan 0 vidas", soft.remainingHits(), 0);
  }
  
  @Test
  public void testGetPoints() {
    assertEquals("Tiene 10 puntos",10,soft.getPoints());
  }
  
  @Test
  public void testPrintBrick() {
    final Printer printer = new Printer();
    final StringBuffer stream = new StringBuffer();
    soft.printBrick(printer, stream);
    assertEquals("Debe imprimir el simbolo *","*",stream.toString());
  }
  
  @Test
  public void testIsDestroyed() {
    assertFalse("No esta destruido",soft.isDestroyed());
  }
  
  @Test
  public void testIsDestroyed2() {
    soft.hit();
    assertTrue("Esta destruido",soft.isDestroyed());
  }
}
