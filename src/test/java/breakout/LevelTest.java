package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import com.cc3002.breakout.logic.level.Level;

import org.junit.Before;
import org.junit.Test;

public class LevelTest {

  private transient Level level;

  
  @Before
  public void setUp() {
    level = new Level(10, 1, "Nivel de prueba");
  }
  
  @Test
  public void testGetNumberBricks() {
    assertEquals("Tiene 10 bricks",10,level.getNumberOfBricks());
  }
  
  @Test
  public void testGetRequiredPoints() {
    assertEquals("Requiere 70 puntos",70,level.getRequiredPoints());
  }
  
  @Test
  public void testSetRequiredPoints() {
    level.setRequiredPoints(20);
    assertEquals("Ahora los puntos requeridos son 20",20,level.getRequiredPoints());
  }
  
  @Test
  public void testInsertBricks() {
    assertEquals("Debe insertar 10 bricks",level.getBricks().size(),10);
  }
  
  @Test
  public void testInsertBricks2() {
    final Level level = new Level(12,0,"Nivel de prueba 2");
    assertEquals("Debe insertar 12 bricks",12,level.getBricks().size());
  }

  @Test
  public void testCreation() {
    assertNotNull("El objeto no debe ser nulo", level);
  }

  @Test
  public void testGetName() {
    assertSame("Debe retornar: Nivel de prueba", level.getName(), "Nivel de prueba");
  }
  
  @Test
  public void testGetBricks() {
    assertEquals("Hay 10 ladrillos",10,level.getBricks().size());
  }
  
  @Test
  public void testGetPoints() {
    assertSame("El nivel tiene 100 puntos",100,level.getPoints());
  }
  
}
