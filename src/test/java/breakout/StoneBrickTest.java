package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.Printer;
import com.cc3002.breakout.logic.brick.IBrick;
import com.cc3002.breakout.logic.brick.StoneBrick;

import org.junit.Before;
import org.junit.Test;


public class StoneBrickTest {
  private transient IBrick stone;
  
  @Before
  public void setUp() {
    stone = new StoneBrick(3);
  }

  @Test
  public void testCreation() {
    assertNotNull("No es nulo", stone);
  }

  @Test
  public void testRemainingHits() {
    assertEquals("Tiene 3 vidas", stone.remainingHits(), 3);
  }

  @Test
  public void testIsStoneBrick() {
    assertTrue("True porque es un stone brick", stone.isStoneBrick());
  }

  @Test
  public void testIsSoftBrick() {
    assertFalse("False porque es un stone brick", stone.isSoftBrick());
  }

  @Test
  public void testHit() {
    stone.hit();
    assertEquals("Le quedan 2 vidas", stone.remainingHits(), 2);
  }
  
  @Test
  public void testGetPoints() {
    assertEquals("Tiene 50 puntos",50,stone.getPoints());
  }

  @Test
  public void testPrintBrick() {
    final Printer printer = new Printer();
    final StringBuffer stream = new StringBuffer();
    stone.printBrick(printer, stream);
    assertEquals("Debe imprimir el simbolo #","#",stream.toString());
  }
}
