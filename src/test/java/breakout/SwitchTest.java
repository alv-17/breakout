package breakout;

import static org.junit.Assert.assertFalse;

import com.cc3002.breakout.logic.AutoSwitchLevel;
import com.cc3002.breakout.logic.Game;
import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.level.Level;

import org.junit.Before;
import org.junit.Test;

public class SwitchTest {

  private transient AutoSwitchLevel auto;
  private transient Game game;
  private transient GameConsole console;

  /**
   * Inicializa variables.
   */
  @Before
  public void setUp() {
    console = new GameConsole();
    auto = new AutoSwitchLevel();
    auto.setConsole(console);
    game = new Game(0, 0, "B");
    game.addNextLevel(new Level(0, 0, "A"));
  }
  
  @Test
  public void testCheck() {
    assertFalse("No tiene los puntos necesrios",auto.check(game));
  }
}
