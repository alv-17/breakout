package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.cc3002.breakout.logic.level.NullLevel;

import org.junit.Before;
import org.junit.Test;

public class NullLevelTest {

  private transient NullLevel noLevel;
  
  @Before
  public void setUp() {
    noLevel = new NullLevel();
  }
  
  @Test
  public void testGetNumberBricks() {
    assertEquals("Tiene 0 bricks",0,noLevel.getNumberOfBricks());
  }

  @Test
  public void testGetName() {
    noLevel.insertBricks(0, 0);
    assertNull("Nulo",noLevel.getName());
  }
  
  @Test
  public void testGetPoints() {
    noLevel.setNextLevel(null);
    assertEquals("0 puntos",0,noLevel.getPoints());
  }
  
  @Test
  public void testGetBricks() {
    noLevel.setRequiredPoints(0);
    assertNull("Nulo",noLevel.getBricks());
  }
  
  @Test
  public void testGetBonuses() {
    noLevel.newBonuses(null);
    assertNull("Nulo",noLevel.getBonuses());
  }
  
  @Test
  public void testGetNextLevel() {
    assertNull("Nulo",noLevel.getNextLevel());
  }

}
