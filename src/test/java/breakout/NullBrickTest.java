package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.GameConsole;
import com.cc3002.breakout.logic.Printer;
import com.cc3002.breakout.logic.brick.NullBrick;

import org.junit.Before;
import org.junit.Test;

public class NullBrickTest {
  
  private transient NullBrick noBrick;

  @Before
  public void setUp() {
    noBrick = new NullBrick();
  }

  @Test
  public void testIsStoneBrick() {
    assertFalse("No es stone brick",noBrick.isStoneBrick());
  }
  
  @Test
  public void testIsSoftBrick() {
    assertFalse("No es soft brick",noBrick.isSoftBrick());
  }
  
  @Test
  public void testGetPoints() {
    assertEquals("Da 0 puntos",0,noBrick.getPoints());
  }
  
  @Test
  public void testIsDestroyed() {
    GameConsole printer = new GameConsole();
    noBrick.printBrickText(printer);
    assertTrue("Esta destruido",noBrick.isDestroyed());
  }
  
  @Test
  public void testRemainingHits() {
    noBrick.hit();
    assertEquals("Quedan 0",0,noBrick.remainingHits());
  }

  @Test
  public void testPintBrick() {
    StringBuffer stream = new StringBuffer();
    Printer printer = new Printer();
    noBrick.printBrick(printer, stream);
    assertEquals("Espacio en blanco"," ",stream.toString());
  }
}
