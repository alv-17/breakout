package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.cc3002.breakout.logic.Player;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

  private transient Player player;
  
  @Before
  public void setUp() {
    player = new Player();
  }

  @Test
  public void testCreation() {
    assertNotNull("No debe ser nulo", player);
  }

  @Test
  public void testGetLife() {
    assertEquals("Debe tener 3 vidas", player.getLife(), 3);
  }
  
  @Test
  public void testGainedLife() {
    player.gainedLife();
    assertEquals("Tiene 4 vidas",4,player.getLife());
  }
 
  @Test
  public void testGetScore() {
    assertEquals("Puntaje del jugador es 0", player.getScore(), 0);
  }

  @Test
  public void testLossLife() {
    player.lossLife();
    assertEquals("El jugador debe haber perdido una vida", player.getLife(), 2);
  }

  @Test
  public void testEarnScore() {
    player.earnScore(50);
    assertEquals("El puntaje debe ser 50", player.getScore(), 50);
  }
}
