package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.cc3002.breakout.logic.bonus.DiscountHeart;
import com.cc3002.breakout.logic.bonus.DiscountPoints;
import com.cc3002.breakout.logic.bonus.ExtraHeart;
import com.cc3002.breakout.logic.bonus.ExtraPoints;

import org.junit.Before;
import org.junit.Test;

public class BonusTest {

  private transient ExtraHeart exHeart;
  private transient ExtraPoints exScore;
  private transient DiscountHeart disHeart;
  private transient DiscountPoints disScore;
  
  /**
   * Inicializa los parametros.
   */
  @Before
  public void setUp() {
    exHeart = new ExtraHeart();
    exScore = new ExtraPoints();
    disHeart = new DiscountHeart();
    disScore = new DiscountPoints();
  }

  @Test
  public void testGetBonus() {
    assertEquals("Tiene 1 vida extra",1,exHeart.getBonus());
  }
  
  @Test
  public void testIsDiscount1() {
    assertFalse("NO es descuento",exScore.isDiscount());
  }
  
  @Test
  public void testIsDiscount2() {
    assertFalse("NO es descuento",exHeart.isDiscount());
  }
  
  @Test
  public void testIsDiscount3() {
    assertTrue("Es descuento",disScore.isDiscount());
  }
  
  @Test
  public void testIsDiscount4() {
    assertTrue("Es descuento",disHeart.isDiscount());
  }
  
  @Test
  public void testIsExtra1() {
    assertFalse("NO es extra",disScore.isExtraBonus());
  }
  
  @Test
  public void testIsExtra2() {
    assertFalse("NO es extra",disHeart.isExtraBonus());
  }
  
  @Test
  public void testIsExtra3() {
    assertTrue("Es descuento",exScore.isExtraBonus());
  }
  
  @Test
  public void testIsExtra4() {
    assertTrue("Es descuento",exHeart.isExtraBonus());
  }

  @Test
  public void testIsHeart1() {
    assertFalse("NO es corazon",disScore.isHeart());
  }
  
  @Test
  public void testIsHeart2() {
    assertTrue("Es corazon",disHeart.isHeart());
  }
  
  @Test
  public void testIsHeart3() {
    assertFalse("NO es corazon",exScore.isHeart());
  }
  
  @Test
  public void testIsHeart4() {
    assertTrue("Es corazon",exHeart.isHeart());
  }
  
  @Test
  public void testIsScore1() {
    assertTrue("Es puntaje",disScore.isScore());
  }
  
  @Test
  public void testIsScore2() {
    assertFalse("NO es puntaje",disHeart.isScore());
  }
  
  @Test
  public void testIsScore3() {
    assertTrue("Es puntaje",exScore.isScore());
  }
  
  @Test
  public void testIsScore4() {
    assertFalse("NO es puntaje",exHeart.isScore());
  }
  
  @Test
  public void testGetScore() {
    assertEquals("Quita 3 puntos",-3,disScore.getScore());
  }
  
  @Test
  public void testGetScore2() {
    assertEquals("No da puntaje",0,exHeart.getScore());
  }
  
  @Test
  public void testGetScore3() {
    assertEquals("No da puntaje",0,disHeart.getScore());
  }

}
