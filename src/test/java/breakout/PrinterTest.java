package breakout;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.cc3002.breakout.logic.Printer;
import com.cc3002.breakout.logic.level.Level;

import org.junit.Before;
import org.junit.Test;

public class PrinterTest {

  private transient Printer printer;
  private transient Level level;
  private transient String expected;

  /**
   * Inicializa los campos.
   */
  @Before
  public void setUp() {
    printer = new Printer();
    level = new Level(18,0,"Nivel de prueba");
    expected = "################\n##";
  }

  @Test
  public void testCreation() {
    assertNotNull("No debe ser nulo el objeto", printer);
  }

  @Test
  public void testPrintStoneBrick() {
    final StringBuffer stream = new StringBuffer();
    printer.printStoneBrick(stream);
    assertEquals("Imprime el caracter #", "#", stream.toString());
  }

  @Test
  public void testPrintSoftBrick() {
    final StringBuffer stream = new StringBuffer();
    printer.printSoftBrick(stream);
    assertEquals("Imprime el caracter *", "*", stream.toString());
  }
  
  @Test
  public void testPrintNullBrick() {
    final StringBuffer stream = new StringBuffer();
    printer.printNullBrick(stream);
    assertEquals("Imprime espacio en blanco", " ", stream.toString());
  }
  
  @Test
  public void testPrint() {
    assertEquals("Debe imprimir ################\n##",expected,printer.print(level.getBricks()));
  }
}
